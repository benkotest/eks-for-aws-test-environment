#EKS Test Environment preparation steps (WIP, will need slight refactoring according to situation and tools provided)

#1 Set up an AWS account and create an IAM user with the necessary permissions to create resources such as EC2 instances, VPCs, and security groups.

#2 EKS CLUSTER FOLDER --> Create an Amazon Elastic Kubernetes Service (EKS) cluster using the AWS Management Console or the AWS CLI. You will need to specify the desired Kubernetes version, region, and instance type for the worker nodes.

#3 VPC FOLDER --> Create an Amazon Virtual Private Cloud (VPC) to host your Kubernetes cluster. You will need to define the CIDR block, subnets, and routing tables.

#4 YAML TEMPLATES --> Set up a testing environment for your application using Kubernetes resources such as Deployments, Services, and ConfigMaps. You can create these resources using YAML files or command-line tools such as kubectl.

#5 Deploy your application to the testing environment using the kubectl command-line tool. You can monitor the progress of your deployment by checking the status of your resources using kubectl.

#6 TESTGRID FOLDER --> Set up a testing framework to automate your testing process. You can use tools such as Kubernetes TestGrid or KubeTest to run tests against your Kubernetes resources. Test needs to be specified, end-to-end/unit/integration!

#7 SCALING FOLDER --> Configure your testing environment to automatically scale your Kubernetes resources based on the workload. You can use the Kubernetes Horizontal Pod Autoscaler (HPA) to scale your Deployments and Pods based on CPU or memory usage.

#8 MONITORING FOLDER --> Monitor the performance of your testing environment using tools such as Kubernetes Dashboard or Prometheus. You can use these tools to track metrics such as resource utilization, network traffic, and application performance.