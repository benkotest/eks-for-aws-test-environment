Log in to the AWS Management Console: Go to the AWS Management Console and log in to your account.

Create a VPC: Navigate to the VPC dashboard and click on the "Create VPC" button. Enter a name and CIDR block for your VPC.

Create subnets: Click on the "Subnets" tab and create at least two subnets in different Availability Zones. Make sure to associate each subnet with your VPC.

Create an Internet Gateway: Click on the "Internet Gateways" tab and create an internet gateway. Attach the internet gateway to your VPC.

Configure routing: Click on the "Route Tables" tab and create a new route table. Associate the route table with your VPC and add a default route that points to the internet gateway.

Create a security group: Click on the "Security Groups" tab and create a new security group. Make sure to allow inbound traffic from your IP address on port 22 for SSH access.

Launch EC2 instances: Launch at least two EC2 instances using an Amazon Machine Image (AMI) that is compatible with Kubernetes. Make sure to select the VPC and subnets you created in previous steps. Also, associate the security group you created in step 6 with these instances.

Configure Kubernetes: Once your EC2 instances are up and running, you need to install and configure Kubernetes. You can use a tool like kops or eksctl to automate this process.

Test your cluster: After you have configured Kubernetes, you can test your cluster by deploying a sample application.