import unittest
from kubernetes import client, config

class TestKubernetesDeployment(unittest.TestCase):
    def setUp(self):
        # Load the Kubernetes configuration
        config.load_kube_config()

    def test_deployment_scale(self):
        # Get the Kubernetes API client
        api = client.AppsV1Api()

        # Define the deployment name and namespace
        deployment_name = "my-deployment"
        namespace = "default"

        # Get the deployment object
        deployment = api.read_namespaced_deployment(deployment_name, namespace)

        # Check the initial replica count
        initial_replicas = deployment.spec.replicas
        self.assertEqual(initial_replicas, 3)

        # Scale the deployment to 5 replicas
        deployment.spec.replicas = 5
        api.patch_namespaced_deployment(deployment_name, namespace, deployment)

        # Check the final replica count
        updated_deployment = api.read_namespaced_deployment(deployment_name, namespace)
        final_replicas = updated_deployment.spec.replicas
        self.assertEqual(final_replicas, 5)

if __name__ == '__main__':
    unittest.main()
