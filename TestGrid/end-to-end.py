import unittest
from kubernetes import client, config

class TestKubernetesEndToEnd(unittest.TestCase):
    def setUp(self):
        # Load the Kubernetes configuration
        config.load_kube_config()

    def test_create_deployment_and_service(self):
        # Get the Kubernetes API client
        api = client.AppsV1Api()
        core_api = client.CoreV1Api()

        # Define the deployment name, namespace, and image
        deployment_name = "my-deployment"
        namespace = "default"
        image = "nginx:latest"

        # Define the service name and port
        service_name = "my-service"
        port = 80

        # Create the deployment object
        deployment = client.V1Deployment()
        deployment.metadata = client.V1ObjectMeta(name=deployment_name)
        deployment.spec = client.V1DeploymentSpec(
            replicas=3,
            selector=client.V1LabelSelector(
                match_labels={"app": deployment_name}
            ),
            template=client.V1PodTemplateSpec(
                metadata=client.V1ObjectMeta(labels={"app": deployment_name}),
                spec=client.V1PodSpec(
                    containers=[client.V1Container(
                        name="nginx",
                        image=image,
                        ports=[client.V1ContainerPort(container_port=port)]
                    )]
                )
            )
        )

        # Create the deployment
        api.create_namespaced_deployment(namespace, deployment)

        # Wait for the deployment to become available
        api.wait_for_namespaced_deployment_ready(deployment_name, namespace)

        # Create the service object
        service = client.V1Service()
        service.metadata = client.V1ObjectMeta(name=service_name)
        service.spec = client.V1ServiceSpec(
            selector={"app": deployment_name},
            ports=[client.V1ServicePort(port=port)]
        )

        # Create the service
        core_api.create_namespaced_service(namespace, service)

        # Wait for the service to become available
        core_api.wait_for_service(service_name, namespace)

        # Verify that the service is accessible
        pod_list = core_api.list_namespaced_pod(namespace, label_selector=f"app={deployment_name}")
        for pod in pod_list.items:
            pod_ip = pod.status.pod_ip
            response = core_api.connect_get_namespaced_service_proxy_with_path(
                name=service_name,
                namespace=namespace,
                path=f"/",
                port=port,
                _preload_content=False,
                host=pod_ip
            )
            self.assertEqual(response.status, 200)

    def tearDown(self):
        # Get the Kubernetes API client
        api = client.AppsV1Api()
        core_api = client.CoreV1Api()

        # Delete the deployment and service
        api.delete_namespaced_deployment("my-deployment", "default")
        core_api.delete_namespaced_service("my-service", "default")

if __name__ == '__main__':
    unittest.main()
