CONFIG-ure HPA: To configure HPA, you will need to create a HorizontalPodAutoscaler resource in Kubernetes. This resource specifies the minimum and maximum number of replicas for your application, as well as the target CPU utilization percentage that should trigger scaling.

This manifest creates an HPA for a deployment named "my-app-deployment" with a minimum of 1 replica and a maximum of 10 replicas. The HPA will scale based on the CPU utilization percentage, with a target of 50%.

Test the HPA: To test the HPA, you can simulate a workload on your application. You can use a tool like Apache Bench or Siege to send a large number of requests to your application. You should see the HPA automatically scale up the number of replicas in response to the increased workload